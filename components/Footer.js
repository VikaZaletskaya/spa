class Footer{
    create(){
        this.element = document.createElement('footer')
        this.element.classList.add('footer')

        this.element.innerHTML=`
        
        <p>Владелец магазина: СООО «Tahia»</p>
        <p>Юр.адрес: Республика Беларусь, г. Минск, 220013, ул. П.Бровки, 22, каб. 210а
        Регистрация №100281103, 04.08.2005, Мингорисполком
        Регистрация в торговом реестре Республики Беларусь от 13.08.2019 УНП 100281103
        Режим работы: пн-пт, 9.00-17.00</p>
        <p>Техническая поддержка:
        +375 29 537 63 90</p>

        `
        return this.element
    }

    init (){
        return this.create()
    }
}

const footer = new Footer().init()
export default footer
