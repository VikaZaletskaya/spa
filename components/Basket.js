function CreateBasket(){
   

    this.create = (basketData) =>{
        this.element=document.querySelector('div')
        this.element.classList.add('basket__container')
       

        if(basketData.length == 0){
            this.element.innerHTML=`<h1>Корзина пуста</h1>`
        }else{
            this.list = ''
            this.totalPrice = 0
            this.element.innerHTML= " "
            basketData.forEach(basketProduct => {
            this.list +=`<div class="product__basket">${basketProduct.title}</div>
                        <div class="product__price">${basketProduct.price} BYN</div>` 
                        this.totalPrice += basketProduct.price*basketProduct.count
            });

            this.element.innerHTML=`<div class="basket">${this.list}</div>
                                    <h2>Total: ${this.totalPrice}</h2>
                                    `
            
        }

        return this.element
    }

    this.init = ()=>{
        this.basketData = JSON.parse(localStorage.getItem('basket')) || []
        return this.create(this.basketData)

    }
}
const basket = new CreateBasket()
export default basket
