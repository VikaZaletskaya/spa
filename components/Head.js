const charset = document.createElement('meta')
const viewport = document.createElement('meta')
const title = document.createElement('title')
const link = document.createElement('link')

charset.setAttribute('charset', 'UTF-8')
viewport.setAttribute('name', 'viewport')
viewport.setAttribute('content', 'width=device-width, initial-scale=1.0')
link.setAttribute('rel', 'stylesheet')
link.setAttribute('href', 'css/style.css')

document.head.appendChild(charset)
document.head.appendChild(viewport)
document.head.appendChild(link)
document.head.appendChild(title)

export {title}